import string

# first approach

print("Enter character number: ")
a = int(input("\t\tNumber = "))
abc = string.ascii_lowercase

if a > len(abc):
    raise TypeError("Index out of bounds!")

print("Characters set: {}".format(abc))

print("\tNeedle character is: {}".format(abc[a-1]))
