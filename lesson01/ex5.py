import string

# first approach

print("Two characters: ")
a = input("\t\tFirst = ")
b = input("\t\tSecond = ")

if len(a) > 1 or len(b) > 1:
    raise TypeError("Please, enter one character for a and b")

abc = string.ascii_letters
ai = abc.index(a)
bi = abc.index(b)
print("Characters set: {}".format(abc))

print("\tIndex of '{}' is : {}".format(a, ai))
print("\tIndex of '{}' is : {}".format(b, bi))
if ai > bi:
    ai, bi = bi, ai
print("Characters between: {}".format(abc[ai:bi+1]))
