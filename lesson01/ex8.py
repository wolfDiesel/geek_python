# -*- coding: utf-8 -*-


print("Leap year detection: ")
a = int(input("\t\tYear = "))

if a % 4 == 0:
    if a % 400 == 0:
        print("Leap year")
    elif a % 100 == 0:
        print("Not a leap")
    else:
        print("Leap year")
