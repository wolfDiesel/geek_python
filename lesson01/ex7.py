# -*- coding: utf-8 -*-

# first approach

print("Enter three lengths: ")
[a, b, c] = [int(input("\t\tLength a = ")), int(input("\t\tLength b = ")), int(input("\t\tLength c = "))]


if a + b < c or a + c < b or b + c < a:
    print("Degenerated triangle")
    exit(0)

if a == b or b == c or a == c:
    if a == b and a == c:
        print("Equilateral triangle")
    else:
        print("Isosceles triangle")
else:
    print("Triangle Vulgaris")
