# -*- coding: utf-8 -*-

# first approach

a = 5
b = 6

print("a OR b = {}".format(a | b))
print("a AND b = {}".format(a & b))
print("a << 2 = {}".format(a << 2))
print("a >> 2 = {}".format(a >> 2))

# second approach

print("a = {0:b}".format(a))
print("b = {0:b}".format(b))
print("a OR b = {0:b}".format(a | b))
print("a AND b = {0:b}".format(a & b))
print("a << 2 = {0:b}".format(a << 2))
print("a >> 2 = {0:b}".format(a >> 2))

