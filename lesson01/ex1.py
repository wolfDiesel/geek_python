# -*- coding: utf-8 -*-

# first approach
nums = list(str(input("Enter 3-digit number: ")))
if len(nums) != 3:
    raise AttributeError("Please, enter exactly 3 digits")

try:
    from functools import reduce

    print("a+b+c={}; a*b*c={}".format(reduce(lambda x, y: int(x) + int(y), nums),
                                      reduce(lambda x, y: int(x) * int(y), nums)))
except Exception as e:
    raise e
