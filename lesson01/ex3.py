# -*- coding: utf-8 -*-

# first approach

print("Coordinates A(x1;y1):")
x1 = float(input("\t\tx1 = "))
y1 = float(input("\t\ty1 = "))

print("Coordinates B(x2;y2):")
x2 = float(input("\t\tx2 = "))
y2 = float(input("\t\ty2 = "))

print("Line equation: ")
k = (y1 - y2) / (x1 - x2)
b = y2 - k * x2
print(" y = %.2f*x + %.2f" % (k, b)) if b > 0 else print(" y = %.2f*x" % k)
